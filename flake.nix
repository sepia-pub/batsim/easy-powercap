{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=23.11";
    flake-utils.url = "github:numtide/flake-utils";
    nur-kapack = {
      url = "github:oar-team/nur-kapack/master";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
    intervalset-flake = {
      url = "git+https://framagit.org/batsim/intervalset";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.nur-kapack.follows = "nur-kapack";
      inputs.flake-utils.follows = "flake-utils";
    };
    batprotocol-flake = {
      url = "git+https://framagit.org/batsim/batprotocol";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.nur-kapack.follows = "nur-kapack";
      inputs.flake-utils.follows = "flake-utils";
    };
  };

  outputs = { self, nixpkgs, flake-utils, nur-kapack, intervalset-flake, batprotocol-flake }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        pkgs = import nixpkgs { inherit system; };
        batprotopkgs = batprotocol-flake.packages.${system};
        intervalsetpkgs = intervalset-flake.packages.${system};
      in rec {
        packages = rec {
          easypower = pkgs.stdenv.mkDerivation rec {
            name = "easypower";
            version = "0.1.0";
            src = pkgs.lib.sourceByRegex ./. [
              "^meson\.build"
              "^.*\.?pp"
              "^.*\.h"
            ];
            buildInputs = [
              pkgs.nlohmann_json
              batprotopkgs.batprotocol-cpp
              intervalsetpkgs.intervalset
            ];
            nativeBuildInputs = [
              pkgs.meson
              pkgs.ninja
              pkgs.pkg-config
            ];
          };
          default = easypower;
        };
        devShells = {
        };
      }
    );
}
